export const FRAPPE_API_GET_USER_INFO_ENDPOINT = '/api/resource/User/';
export const CLIENT_CALLBACK_ENDPOINT = '/home/callback';
export const FRAPPE_API_GET_OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token/';
